public class Main {
    static final String ERROR = "Error! The data entered is incorrect.";

    public static void main(String[] args) {
        if(args.length == 4) {
            try {
                final int A = Integer.parseInt(args[0]);
                final int P = Integer.parseInt(args[1]);
                final double M1 = Double.parseDouble(args[2]);
                final double M2 = Double.parseDouble(args[3]);
                Double gravity = GravityCalculator.calculate(A, P, M1, M2);
                System.out.println(gravity);
            } catch (NumberFormatException e) {
                System.out.println(ERROR);
            }
        } else {
                System.out.println(ERROR);
            }
        }
    }