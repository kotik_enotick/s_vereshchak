/**
 * Calculating value of gravity
 * @author Sergei Vereshchak
 * @author kotik.enotick@gmail.com
 */
class GravityCalculator {
    /**
     * Calculating value of gravity
     * @return value of gravity
     */
    public static Double calculate(int A, int P, double M1, double M2) {
        return (4 * Math.pow(Math.PI, 2) * Math.pow(A, 3)) / (Math.pow(P, 2) * (M1 + M2));
    }
}
