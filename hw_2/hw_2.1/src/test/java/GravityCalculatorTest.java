import org.junit.Test;


import static org.junit.Assert.*;

public class GravityCalculatorTest {

    @Test
    public void testCalculate() {
        String[] args = {"1", "1", "1", "1"};
        double result = GravityCalculator.calculate(1 ,1, 1.0, 1.0);
        double expectResult = 19.739208802178717237635764265786;
        assertEquals(expectResult, result, 0);
    }
}