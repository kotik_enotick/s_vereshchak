import org.junit.Before;
import org.junit.Test;
import org.junit.After;

import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class MainTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    final String ERROR = "Error! The data entered is incorrect.";

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }

    @Test
    public void testValidArgs() {
            String[] args = {"1", "1", "1", "1"};
            Main.main(args);
            String result = outContent.toString();
            System.out.println(result);
            assertEquals( "19.739208802178716\r\n", result);
        }

    @Test
    public void testInvalidArgs() {
        String[] args = {"1", "asd", "1", "1"};
        Main.main(args);
        String result = outContent.toString();
        assertEquals(ERROR + "\r\n", result);
    }

    @Test
    public void testWrongNumberOfArgs() {
        String[] args = {"1", "asd", "1", "1", "hehehe"};
        Main.main(args);
        String result = outContent.toString();
        assertEquals(ERROR + "\r\n", result);
    }
}
