import org.junit.Test;

import static org.junit.Assert.*;

public class FibonacciNumbersTest {
    FibonacciNumbers fibNum = new FibonacciNumbers();

    @Test
    public void testStartAlgorithm() {
        int result = fibNum.startAlgorithm(1, 5);
        int expect = 5;
        assertEquals(expect, result);

        result = fibNum.startAlgorithm(2, 5);
        assertEquals(expect, result);

        result = fibNum.startAlgorithm(3, 5);
        assertEquals(expect, result);
    }

    @Test
    public void testStartAlgorithmWithWrongArgs() {
        int result = fibNum.startAlgorithm(-1,  0);
        int expectError = -1;
        assertEquals(expectError, result);
    }
}
