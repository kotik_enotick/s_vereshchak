import org.junit.Test;

import static org.junit.Assert.*;

public class FactorialTest {
    Factorial fact = new Factorial();

    @Test
    public void testStartAlgorithm() {
        int result  = fact.startAlgorithm(1, 5);
        int expect = 120;
        assertEquals(expect, result);

        result = fact.startAlgorithm(2, 5);
        assertEquals(expect, result);

        result = fact.startAlgorithm( 3, 5);
        assertEquals(expect, result);
    }

    @Test
    public void testStartAlgorithmWithWrongArgs() {
        int result = fact.startAlgorithm(-1,  0);
        int expectError = -1;
        assertEquals(expectError, result);
    }
}
