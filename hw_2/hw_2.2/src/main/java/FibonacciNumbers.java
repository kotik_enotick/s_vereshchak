public class FibonacciNumbers extends Algorithm {
    private int fib = 0, fib1 = 0, fib2 = 1, i = 0;

    /**
     * Execute fibonacci number with numberN using a loop do-while
     * @param numberN the number with witch the algorithm is executed. Can't be negative
     * @return algorithm execute result
     */
    @Override
    protected int loopFor(int numberN) {
        for (; i < numberN; i++) {
            if (i == 0) {
                fib = fib1;
                continue;
            }
            fib = fib1 + fib2;
            fib1 = fib2;
            fib2 = fib;
        }
        return fib;
    }

    /**
     * Execute fibonacci number with numberN using a loop do-while
     * @param numberN the number with witch the algorithm is executed. Can't be negative
     * @return algorithm execute result
     */
    @Override
    protected int loopDoWhile(int numberN) {
        do {
            if(i == 0) {
                fib = fib1;
                i++;
                continue;
            }
            fib = fib1 + fib2;
            fib1 = fib2;
            fib2 = fib;
            i++;
        } while (i < numberN);
        return fib;
    }

    /**
     * Execute fibonacci number with numberN using a loop while
     * @param numberN the number with witch the algorithm is executed. Can't be negative
     * @return algorithm execute result
     */
    @Override
    protected int loopWhile(int numberN) {
        while (i < numberN) {
            if(i == 0) {
                fib = fib1;
                i++;
                continue;
            }
            fib = fib1 + fib2;
            fib1 = fib2;
            fib2 = fib;
            i++;
        }
        return fib;
    }
}
