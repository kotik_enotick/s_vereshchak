class Factorial extends Algorithm {
    private static int fact = 1;

    /**
     * Execute factorial with numberN using a loop while
     * @param numberN the number with witch the algorithm is executed. Can't be negative
     * @return algorithm execute result
     */
    @Override
    protected int loopWhile(int numberN) {
        while (numberN >= 1) {
            fact *= numberN;
            numberN--;
        }
        return fact;
    }

    /**
     * Execute factorial with numberN using a loop do-while
     * @param numberN the number with witch the algorithm is executed. Can't be negative
     * @return algorithm execute result
     */
    @Override
    protected int loopDoWhile(int numberN) {
        do {
            fact *= numberN;
            numberN--;
        } while(numberN > 1);
        return fact;
    }

    /**
     * Execute factorial with numberN using a loop do-while
     * @param numberN the number with witch the algorithm is executed. Can't be negative
     * @return algorithm execute result
     */
    @Override
    protected int loopFor(int numberN) {
        for (; numberN >= 1 ; numberN--) {
            fact *= numberN;
        }
        return fact;
    }
}
