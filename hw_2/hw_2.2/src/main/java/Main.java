import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int algorithmType, loopType, numberN, result;

        while (true) {
            result = -1;
            try {
                System.out.println("Select type of algorithm.\n1. Fibonacci numbers\n2. Factorial calculation");
                algorithmType = readNumber();
                System.out.println("Select a cycle type.\n1. while\n2. do-while\n3. for");
                loopType = readNumber();
                System.out.println("Enter parameter n (The first n numbers for the Fibonacci numbers," +
                        " Factorial of the number n for calculating the factorial)");
                numberN = readNumber();
            } catch (InputMismatchException e) {
                System.out.println("The data entered is incorrect. Please, try again");
                continue;
            }

            //choose the necessary algorithm
            if (algorithmType == 1) {
                Factorial fact = new Factorial();
                result = fact.startAlgorithm( loopType, numberN);
            } else if (algorithmType == 2) {
                FibonacciNumbers fibNum = new FibonacciNumbers();
                result = fibNum.startAlgorithm(loopType, numberN);
            }
            System.out.print("Result is ");
            System.out.println(result);
        }
    }

    /**
     * read the number
     * @return read int value
     * @throws InputMismatchException
     */
    private static int readNumber() throws InputMismatchException {
        Scanner scan = new Scanner(System.in);
        return scan.nextInt();
    }
}
