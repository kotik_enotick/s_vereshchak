abstract class Algorithm  {
    /**
     * Runs the checks and choose the loop
     * @param loopType type of executable loop.
     * @param numberN the number with witch the algorithm is executed. Can't be negative
     * @return algorithm execute result
     */
    public int startAlgorithm(int loopType, int numberN) {
        if(!checkValidParams(loopType, numberN)) {
            return -1;
        }

        if (loopType == 1) {
            return loopWhile(numberN);
        } else if (loopType == 2) {
            return loopDoWhile(numberN);
        } else {
            return loopFor(numberN);
        }
    }

    /**
     * Checked loopType and numberN on valid values
     * @param loopType type of executable loop. Can't be more than 3 and less than 1
     * @param numberN the number with witch the algorithm is executed. Can't be negative
     * @return true if check good, and false if check bad
     */
    protected static boolean checkValidParams( int loopType, int numberN) {
        if ( loopType < 1 || loopType > 3 || numberN < 0) {
            return false;
        }
        return true;
    }

    /**
     * Execute algorithm with numberN using a loop while
     * @param numberN the number with witch the algorithm is executed. Can't be negative
     * @return algorithm execute result
     */
    protected int loopWhile(int numberN) {
        return -1;
    }

    /**
     * Execute algorithm with numberN using a loop do-while
     * @param numberN the number with witch the algorithm is executed. Can't be negative
     * @return algorithm execute result
     */
    protected int loopDoWhile(int numberN) {
        return -1;
    }

    /**
     * Execute algorithm with numberN using a loop do-while
     * @param numberN the number with witch the algorithm is executed. Can't be negative
     * @return algorithm execute result
     */
    protected int loopFor(int numberN) {
        return -1;
    }
}
